import React from "react";
import Slider from "react-slick";
import Slide from "../components/Slide";

/*The SimpleSlider component wraps a slick slider component
and renders a list of slide elements inside the slider*/
export default class SimpleSlider extends React.Component {
  componentWillMount() {
    /*The onMount prop will run when the component is about to mount
      in this application, onMount is a method to scroll the page
      to this element to draw the users attention to it*/
    this.props.onMount();
  }

  /*generateSlides will map the passed in data onto a list of
  Slide components and creates a relevant clickHandler for each Slide instance*/
  generateSlides = () => {
    return this.props.data.map((item,i) => {
      return(< Slide selected={(this.props.selectedIngredient === item.ingredient ? true : false)} title={item.ingredient} imgURL={item.imgURL} clickHandler={()=>{this.props.selectFunction(item.ingredient)}}/>);
    })
  };

  render() {
    /*Basic setup for the slick slider class*/
    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: this.props.slidesToShow,
      slidesToScroll: 1
    };

    return (
      <Slider {...settings}>
        {this.generateSlides()}
      </Slider>
    );
  }
}

/*By default, the slidesToShow prop is set to 1, this means
in the event of an error passing in slidesToShow prop, only one slide
will be rendered at a time which is a safe fallback*/
SimpleSlider.defaultProps = {
    slidesToShow: 1,
    onMount: function(){}
};
