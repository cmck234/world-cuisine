import React, { Component } from 'react';

/*The NavItem component turns its child property into a list item
that can be passed to a NavBar/NavBarMobile component.
*/
export default class NavItem extends Component {
  render(){
    return(<li className="nav-item link-hover">
      <a className="text-muted nav-link" href="#">
        {/*Rendering a child element instead of a text property
        means you can pass non-text components, such as <i> elements*/}
        {this.props.children}
      </a>
    </li>);
  }
}
