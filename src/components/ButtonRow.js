import React, { Component } from 'react';

/*The ButtonRow component wraps a standard boostrap row
  and will be used to render groups of buttons and card elements
*/
export default class ButtonRow extends Component {
  render() {
    return(
      <div className="row fixed-row align-items-start">
        {/*The children should have the 'col' css class
          to be formatted with the Bootstrap grid*/}
        {this.props.children}
      </div>
    );
  }
}
