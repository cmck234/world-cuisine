import React, { Component } from 'react';

/*The IngredientButton component is used to create a formatted button
given a clickHandler, selected, title and content prop
This should be passed to a ButtonRow component to make use of bootstraps grid layout*/
export default class IngredientButton extends Component {
  render() {
    return(
      <div className="col-sm align-top Aligner" onClick={this.props.clickHandler}>
        {/*If the selected ingredient is this one, add the
          'selected' class to the classname*/}
        <div className={"Aligner-item Aligner flag-button " + (this.props.selected === true? "selected" : "")}>
        <div className="Align-item" style={{width:'100%',textAlign:'center'}}>
        <img src={this.props.imgURL} alt={this.props.title} style={{width:'120px', height:'120px'}} />
        </div>
          <h6 className="Align-item" style={{paddingTop:'17px'}}>{this.props.title}</h6>
            <p  className="flag-button-content Align-item"><small>{this.props.content}</small></p>
        </div>
      </div>
    )
  }
}
