import React, { Component } from 'react';

/*Jumbotron is a generic jumbotron component, which wraps
the bootstrap jumbotron class and renders its child components
inside itself*/
export default class Jumbotron extends Component {
  render() {
    return(
      <div className="jumbotron jumbotron-fluid" style={{textShadow: "3px 5px 5px #666"}}>
        <div className="container-fluid vertical-center">
          {/*Rendering child components instead of fixed title and content props
            gives the component some flexibility, as any amount or type of
            components can be passed to it*/}
          {this.props.children}
        </div>
      </div>);
  }
}
