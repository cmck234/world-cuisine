import React, { Component } from 'react';

/*The Slide component is used to format an image and title to be passed
  to the IngredientSlider component as a child element*/
export default class Slide extends Component {
  render() {
    return(
      <div className="col-sm  align-top Aligner" onClick={this.props.clickHandler}>
        {/*If this Slide has the selected prop, give it the selected css class*/}
        <div className={"Aligner-item Aligner flag-button " + (this.props.selected === true? "selected" : "")}>
        <div className="Align-item" style={{width:'100%',textAlign:'center'}}>
        <img className="Align-item" alt={this.props.title} src={this.props.imgURL} style={{transform:'translateX(60px)',width:'120px', height:'120px'}} />
        </div>
          <h6 className="Align-item" style={{paddingTop:'17px'}}> {this.props.title}</h6>
        </div>
      </div>
    )
  }
}
