import React, { Component } from 'react';

/*The NavBar component is the desktop navbar which renders the business name
on the left, and nav buttons on the right*/
export default class NavBar extends Component {
  render() {
    return(
    <div className="header clearfix">
      <nav>
        <ul className="nav nav-pills float-right">
        {/*The navbar items are passed in as children and
          can be any element, but should be a list of NavItems to render properly*/}
          {this.props.children}
        </ul>
      </nav>
      <h3 className="text-white luxus-logo">LUXUS</h3>
    </div>);
  }
}
