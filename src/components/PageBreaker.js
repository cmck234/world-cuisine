import React, { Component } from 'react';

/*The PageBreaker component is a generic container class to render a styled
  div that takes up the full width of the App container*/
export default class PageBreaker extends Component {
  render() {
    return(
      <div style={this.props.style} className="container-fluid centered-text">
        {this.props.children}
      </div>
    )
  }
}
