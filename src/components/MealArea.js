import React, {Component} from 'react';
import ButtonRow from '../components/ButtonRow';
import MealCard from '../components/MealCard';

/*The MealArea component renders a list of MealCard components in a bootstrap grid*/
export default class MealArea extends Component {
  componentWillMount() {
    /*The onMount prop will run when the component is about to mount
      in this application, onMount is a method to scroll the page
      to this element to draw the users attention to it*/
    this.props.onMount();
  }

  /*The generateCards method will generate a dummy list of MealCard elements
    to be rendered*/
  generateCards = () => {
    let elems = [1,2,3,4,5,6];
      return elems.map((elem) => <MealCard key={elem} title={`Example meal ${elem}`} content={'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'} imgURL={'https://dummyimage.com/120x90/969196/fff'}/>)
  }

  render() {
    return(
      <div style={this.props.style} className="container-fluid bg-white">
      <ButtonRow>
        {this.generateCards()}
      </ButtonRow>
      </div>
    )
  }
}

MealArea.defaultProps = {
  onMount: function() {}
}
