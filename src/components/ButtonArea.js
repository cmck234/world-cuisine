import React, { Component } from 'react';
import ButtonRow from '../components/ButtonRow';
import Button from '../components/Button';
import IngredientButton from '../components/IngredientButton';

/*The ButtonArea component turns a data array into a list of Button elements,
  rendered in a bootstrap grid*/
export default class ButtonArea extends Component {
  componentWillMount() {
    /*The onMount prop will run when the component is about to mount
      in this application, onMount is a method to scroll the page
      to this element to draw the users attention to it*/
    this.props.onMount();
  }

  renderData = () => {
    /*We can figure out which button to render based on what properties the data has*/
    if(this.props.data[0].hasOwnProperty('country')) {
      /*if the data is an array of country objects, render the country info and flag image*/
      return this.props.data.map((item,i) => {
        return(< Button key={i} selected={(this.props.selectedCountry === item.country ? true : false)} title={item.country} content={item.content} flagISO={item.flagISO} clickHandler={()=>{this.props.selectFunction(item.country)}}/>);
      })
    }
    else {
      /*else, render the ingredient buttons*/
      return this.props.data.map((item,i) => {
        return(< IngredientButton key={i} selected={(this.props.selectedMealType === item.mealType ? true : false)} title={item.mealType} content={item.content} imgURL={item.imgURL} clickHandler={()=>{this.props.selectFunction(item.mealType)}}/>);
      })
    }
  }

  render() {
    return(<div style={this.props.style} className="container-fluid bg-white">
             <ButtonRow>
              {this.renderData()}
              </ButtonRow>
          </div>
    );
  }
}

ButtonArea.defaultProps = {
  onMount : function(){}
}
