import React, { Component } from 'react';


/*The NavBarMobile Component is the mobile navbar which places
  each element on its own line, with the business name centred at the top*/
export default class NavBarMobile extends Component {
  render() {
    return(
      <div className="container container-fluid">
      <h3 className="text-white luxus-logo">LUXUS</h3>
      <nav>
        <ul className="nav nav-pills float-center" >
          {/*The navbar items are passed in as children and
            can be any element, but should be a list of NavItems to render properly*/}
          <div style={{width:'100%'}}>
          {this.props.children}
          </div>
        </ul>
      </nav>
      </div>
  );
  }
}
