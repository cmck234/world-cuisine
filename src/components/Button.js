import React, { Component } from 'react';
import 'flag-icon-css/css/flag-icon.css';

/*The Button component renders a card style element with a flag, content and title
  which expands if the user hovers over it, and has a clickHandler passed to it by its container*/
export default class Button extends Component {
  render() {
    /*Determine the correct CSS flag class based on the Button's
      flagISO prop using string interpolation - just add the flag ISO code
      to the flag-icon- string*/
    let flagClass = `circleBase flag-icon Align-item flag-icon-${this.props.flagISO}`;

    return(
      <div className="col-sm  align-top Aligner" onClick={this.props.clickHandler}>
      {/*If this Button has the selected prop, give it the selected css class*/}
        <div className={"Aligner-item Aligner flag-button " + (this.props.selected === true? "selected" : "")}>
        <div className="Align-item" style={{width:'100%',textAlign:'center'}}>
        <div className={flagClass} style={{width:'120px', height:'120px', backgroundSize:'cover'}}></div>
        </div>
          <h6 className="Align-item" style={{paddingTop:'17px'}}>{this.props.title}</h6>
            <p  className="flag-button-content Align-item"><small>{this.props.content}</small></p>
        </div>

      </div>
    )
  }
}
