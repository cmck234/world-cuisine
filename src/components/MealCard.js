import React, { Component } from 'react';

/*The MealCard Component renders a card style component
that expands to show its content when the user hovers over it*/
export default class MealCard extends Component {
  render() {
    return(
      <div className="col-sm  align-top Aligner" >
        <div className={"Aligner-item Aligner meal-card "}>
        <div class="Align-item" style={{width:'100%',textAlign:'center'}}>
        <img src={this.props.imgURL} alt={this.props.title} style={{width:'100%', height:'auto'}} />
        </div>
          <h6 className="Align-item" style={{paddingTop:'17px'}}>{this.props.title}</h6>
            <p  className="meal-card-content Align-item"><small>{this.props.content}</small></p>
        </div>
      </div>
    );
  }
}
