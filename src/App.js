import React, { Component } from 'react';
import scrollToComponent from 'react-scroll-to-component';
import './App.css';
import PageBreaker from './components/PageBreaker';
import NavItem from './components/NavItem';
import NavBar from './components/NavBar';
import NavBarMobile from './components/NavBarMobile';
import Jumbotron from './components/Jumbotron';
import ButtonArea from './components/ButtonArea';
import IngredientSlider from './components/IngredientSlider';
import MealArea from './components/MealArea';
import MediaQuery from 'react-responsive';

/*The following arrays are dummy data, which would otherwise come in via an API
  or from the back-end*/
var countryData =
  [
    {country: 'France', flagISO: 'fr', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tellus ligula, luctus at tellus in, luctus suscipit orci.'},
    {country: 'Spain', flagISO: 'es', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tellus ligula, luctus at tellus in, luctus suscipit orci.'},
    {country: 'Italy', flagISO: 'it', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tellus ligula, luctus at tellus in, luctus suscipit orci.'},
    {country: 'Japan', flagISO: 'jp', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tellus ligula, luctus at tellus in, luctus suscipit orci.'},
    {country: 'China', flagISO: 'cn', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tellus ligula, luctus at tellus in, luctus suscipit orci.'},
    {country: 'Brazil', flagISO: 'br', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tellus ligula, luctus at tellus in, luctus suscipit orci.'}
  ];

var mealTypes = [
  {mealType: 'Breakfast & Brunch', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ', imgURL: 'https://dummyimage.com/120x120/969196/fff'},
  {mealType: 'Appetisers & Snacks', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ', imgURL: 'https://dummyimage.com/120x120/969196/fff'},
  {mealType: 'Lunch & Salads', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ', imgURL: 'https://dummyimage.com/120x120/969196/fff'},
  {mealType: 'Dinner', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ', imgURL: 'https://dummyimage.com/120x120/969196/fff'},
  {mealType: 'Desserts', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ', imgURL: 'https://dummyimage.com/120x120/969196/fff'},
  {mealType: 'Drinks & Smoothies', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ', imgURL: 'https://dummyimage.com/120x120/969196/fff'}
];

var ingredients = [
  {ingredient: 'Chicken', imgURL: 'https://dummyimage.com/120x120/969196/fff'},
  {ingredient: 'Pasta', imgURL: 'https://dummyimage.com/120x120/969196/fff'},
  {ingredient: 'Cheese', imgURL: 'https://dummyimage.com/120x120/969196/fff'},
  {ingredient: 'Vegetables', imgURL: 'https://dummyimage.com/120x120/969196/fff'}
]

class App extends Component {
  constructor(props) {
    super(props);
    /*Initialise the empty state, these values will be set when the user
      makes their country, meal and ingredient selection*/
    this.state = {
      selectedCountry : '',
      selectedMealType: '',
      selectedIngredient: ''
    }
  }

  /*The following 3 functions set the 3 pieces of state to a parameter passed
    to them, they will be passed down into containers and then into button elements
    */
  selectCountry = ( country ) => {
    this.setState({selectedCountry : country})
  }

  selectMealType = ( mealType ) => {
    this.setState({selectedMealType : mealType})
  }

  selectIngredient = ( ingredient ) => {
    this.setState({selectedIngredient : ingredient})
  }

  render() {
    return (
      <div className="container App">
      {/*With the MediaQuery element, you can alter the app structure programmatically
      rather than with CSS, in this case either a NavBar or NavBarMobile is rendered
      depending on the screen width*/}
      <MediaQuery minWidth={800}>
      <NavBar>
        <NavItem>
          Recipes
        </NavItem>
        <NavItem>
          Seasonal
        </NavItem>
        <NavItem>
          Blog
        </NavItem>
        <NavItem>
          Contact
        </NavItem>
        <NavItem>
          <i className="fa fa-search"></i>
        </NavItem>
      </NavBar>
      </MediaQuery>
      <MediaQuery maxWidth={800}>
      <NavBarMobile>
        <NavItem>
          Recipes
        </NavItem>
        <NavItem>
          Seasonal
        </NavItem>
        <NavItem>
          Blog
        </NavItem>
        <NavItem>
          Contact
        </NavItem>
        <NavItem>
          <i class="fa fa-search"></i>
        </NavItem>
      </NavBarMobile>
      </MediaQuery>

      <Jumbotron>
      <span className="jumbo-text">
        <h1  className="display-4 text-white font-weight-bold jumbotitle">It is time to unlock<br/>
        <span className="block-underline">your creativity</span></h1>
        <p className="lead text-white jumbocontent">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tellus ligula, luctus at tellus in, luctus suscipit orci. </p>
      </span>
      </Jumbotron>

      <PageBreaker style={{backgroundColor:'#ffffff'}} >
        <div style={{display:'flex', flexWrap:'wrap',justifyContent:'center'}}>
        <span  className="input-group-addon text-muted"><small>Browse by:</small></span>
        <select className="form-control" id="sel1">
          <option value="mtype">Meal type</option>
          <option value="ingredients">Ingredients</option>
          <option value="wcuisine">World cuisine</option>
          <option value="cstyle">Cooking style</option>
        </select>
        </div>
      </PageBreaker>

      <PageBreaker style={{backgroundColor:'#17a2b8', color:'white'}}>
        Where do you want to travel?
      </PageBreaker>

      <ButtonArea
      data={countryData}
      selectFunction={this.selectCountry}
      selectedCountry={this.state.selectedCountry}/>

      {/*With inline conditional rendering, render the meal type selection span
        only if the country has been selected*/}
      { this.state.selectedCountry !== '' ? (
        <span>
        <PageBreaker style={{backgroundColor:'#54dd76', color:'white'}}>
          What do you want to cook today?
        </PageBreaker>
        <ButtonArea onMount={()=>{
            scrollToComponent(this.mealTypeSection, { offset: 0, align: 'middle', duration: 400, ease:'inExpo'});}}
            data={mealTypes}
            selectFunction={this.selectMealType}
            selectedMealType={this.state.selectedMealType}/>
        </span>
      ) : (
        null
      )}
      {/*the span references will be used by the scroll element, to
        scroll to this area of the DOM */}
      <span ref={(span) => { this.mealTypeSection = span; }}>
      </span>


      {/*With inline conditional rendering, render the ingredient selection span
        only if the meal type has been selected*/}
      { this.state.selectedMealType !== '' ? (
        <span>
        <PageBreaker style={{backgroundColor:'#4f4d4a', color:'white'}}>
          What would you like as your main ingredient?
        </PageBreaker>
        <MediaQuery minWidth={800}>
          <IngredientSlider
          onMount={()=>{
              scrollToComponent(this.ingredientSection, { offset: 0, align: 'middle', duration: 400, ease:'inExpo'});}}
          data={ingredients}
          slidesToShow={3}
          selectFunction={this.selectIngredient}
          selectedIngredient={this.state.selectedIngredient}/>
        </MediaQuery>
        <MediaQuery maxWidth={800}>
          <IngredientSlider
          onMount={()=>{
              scrollToComponent(this.ingredientSection, { offset: 0, align: 'middle', duration: 400, ease:'inExpo'});}}
          data={ingredients} slidesToShow={1}
          selectFunction={this.selectIngredient}
          selectedIngredient={this.state.selectedIngredient}/>
        </MediaQuery>

        </span>
      ) : (
        null
      )}
      {/*the span references will be used by the scroll element, to
        scroll to this area of the DOM */}
      <span ref={(span) => { this.ingredientSection = span; }}>
      </span>


      {/*With inline conditional rendering, render the meal list span
        only if the ingredient has been selected*/}
      { this.state.selectedIngredient !== '' ? (
        <span>
          <PageBreaker style={{backgroundColor:'#384e4f', color:'white'}}>
            {this.state.selectedIngredient} based meals from {this.state.selectedCountry}
          </PageBreaker>
          <MealArea
          onMount={()=>{
              scrollToComponent(this.mealSection, { offset: 0, align: 'bottom', duration: 400, ease:'inExpo'});}}
          />
        </span>
      ) : (
        null
      )}


      <footer className="bd-footer text-muted">
        <PageBreaker style={{backgroundColor:'#4f4d4a', color:'white'}}>
        2018
        </PageBreaker>
      </footer>
      {/*the span references will be used by the scroll element, to
        scroll to this area of the DOM */}
      <span ref={(span) => { this.mealSection = span; }}>

      </span>
      </div>
    );
  }
}

export default App;
